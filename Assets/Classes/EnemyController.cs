﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {


    public int health = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
	}

    public void GetHit(int damage){

        health -= damage;

        Debug.Log(health);
    }
}
