﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private Rigidbody rb;
    public float speed;

    public int damage;

    private void Awake()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    public void Fire()
    {
        rb.AddForce(transform.forward * speed);
    }

    public void OnTriggerEnter(Collider collider){

        if(collider.gameObject.tag == "Enemy"){

            EnemyController enemy = collider.gameObject.GetComponent<EnemyController>();
            enemy.GetHit(damage);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {

            EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
            enemy.GetHit(damage);
            Destroy(this.gameObject);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
