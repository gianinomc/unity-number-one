﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

    public GameObject projectile;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if (Input.GetButtonDown("Fire1")){

            Vector3 playerPos = transform.position;
            Vector3 playerDirection = transform.forward;
            Quaternion playerRotation = transform.rotation;

            Vector3 spawnPos = playerPos + playerDirection * 1;

            GameObject bullet = Instantiate(projectile, spawnPos , playerRotation);
            bullet.GetComponent<Projectile>().Fire();

        }
	}
}
